from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Daffa Muhammad Rayhan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,8,28) #TODO Implement this, format (Year, Month, Date)
npm = "1706026954" # TODO Implement this
describe = "I am someone who is humble, likes to help, outgoing, humorous, brave and creative. I can sometimes be an introvert and can also be an extrovert. Yeahh , it's me :) "

#Front friends
f_name = "Naufal Pratama Tanansyah"
year_f = int(datetime.now().strftime("%Y"))
birt_f = date(1999,10,21)
npm_f = "1706979410"
describe_f = "Dulu aku mau masuk Fasilkom karena aku ingin mengembangkan sesuatu di bidang ini. Sekarang, setelah aku masuk Fasilkom, aku malah jadi receh. Yang aku lakukan kalo aku lagi bosen biasanya menggambar, nonton YouTube, atau main game payah yang referensinya aku dapet dari iklan aplikasi HP. "

#Back friends
b_name = "Salsabila Hava Qabita"
year_b = int(datetime.now().strftime("%Y"))
birt_b = date(2000,1,8)
npm_b = "1706979461"
describe_b = "Hobi gue fangirling, dengerin musik, baca, dan nyanyi. Personality type gue INFJ, jadinya pendiem gitu terus kadang suka awkward hehe. Things i enjoy: music, films, food, tv shows, cars, formula one, talking about social issues, and napping."

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm ,'describe' : describe, "frontname" : f_name,"fage": calculate_age(birt_f.year), "frontnpm" : npm_f, "Fdescribe":describe_f, "backname": b_name,"bage": calculate_age(birt_b.year), "backnpm": npm_b, "Bdescribe": describe_b }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0